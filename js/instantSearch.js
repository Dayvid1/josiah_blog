var search = instantsearch({
	urlSync:true,
	searchParameters:{
		searchResultsPerPage:10
	}

});


search.addWidget(instantsearch.widgets.searchBox({
	container: '#searchField-player'
})
);

search.addWidget(
	instantsearch.widgets.searchResults({
		div: '#searchResults',
		templates: {
			item:document.getElementById('audio-download').innerHTML,
			empty: "We didn't find any results for the search <em> \"{{q}}\"</em>"
		}
	})
);

search.start();