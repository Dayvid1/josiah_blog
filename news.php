<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    

  

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->
    <link href="css/new-age.min.css" rel="stylesheet">
    <style type="">
      #mainNav{
        background-color: purple;
      }

      section.tech-feature{
      padding-top: 50px;
      padding-bottom: 50px;
      text-align: center; }
    </style>

    
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Updated News</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">HOME</a>
            </li>
              
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#entertainment">ENTERTAINMENT</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#technology">TECHNOLOGY</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#sport">SPORT</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">CONTACT</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>


    <!-- Entertainment Profile -->

    <header class="head">
<section class="features" id="entertainment">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Follow my Blog</h2>
          <p class="text-muted">News around the world, never miss any information!</p>
          <hr>
        </div>
        <div class="row">
          <div class="col-lg-4 my-auto">
            <div class="device-container">
              <div class="ads">
                <div class="device">
                  <div class="ads-background" style="background-size: 30px; height: 400px; padding: 50px;">
                  <img src="">
                    <h3>Latest Albums</h3>
                    <p>Banky W Album()</p>
                    <p>Wizkid Album()</p>
                    <p>Davido Album()</p>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-money"></i>
                    <h3>Join and Bet</h3>
                    <p class="text-muted">Ready to start earning!</p>
                    <a href="http://www.bet.co.za" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <h3>Sale Offers</h3>
                    <p class="text-muted">Register and purchase online</p>
                    <a href="http://www.cashcrate.com/7517294" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-credit-card"></i>
                    <h3>MasterCard Security</h3>
                    <p class="text-muted">Secure your account!</p>
                    <a href="https://voguepay.com/
                    1766-0054927
                    " style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-film"></i>
                    <h3>Moviewatcher</h3>
                    <p class="text-muted">Watch any movie by downloading or streaming!</p>
                    <a href="http://www.bmovies.is" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    </header>


    <!-- Technology profile -->
    <section class="download bg-primary text-center" id="technology">
      <div class="container">
        <div class="row">
       
        <div class="col-lg-12">
           <h3>THE TECHNOLOGY WORLD!</h3>
           <p>Build Your Technology Information</p>
            <div class="col-md-6  pull-left">
            
              <div class="tech-feature" style="background-size: 30px; height: 250px; padding: 50px;">
                    <h3>Xclaimlabs</h3>
                    <p>Be secured with your site.</p>
                    <a href="http://www.xclaimlabs.com" style="text-decoration: none;">Click to connect</a>
              </div>
            </div>

             <div class="col-md-6 pull-right">
               <div class="tech-feature" style="background-size: 30px;  height: 250px; padding: 50px;">
                    <h3>Xclaimlabs</h3>
                    <p>Be secured with your site.</p>
                    <a href="http://www.xclaimlabs.com" style="text-decoration: none;">Click to connect</a>
                 </div>
              </div>
           
            <div class="col-md-6  pull-left">
            
              <div class="tech-feature" style="background-size: 30px;  height: 250px; padding: 20px;">
               
                    <h4>Xclaimlabs</h4>
                    <p>Be secured with your site.</p>
                    <a href="http://www.xclaimlabs.com" style="text-decoration: none;">Click to connect</a>
              </div>
            </div>

             <div class="col-md-6 pull-right">
               <div class="tech-feature" style="background-size: 30px; height: 250px; padding: 50px;">
                    <h3>Xclaimlabs</h3>
                    <p>Be secured with your site.</p>
                    <a href="http://www.xclaimlabs.com" style="text-decoration: none;">Click to connect</a>
                 </div>
              </div>
      
        </div>
      
        </div>
      </div>
      
    </section>

    <!-- Sport profile -->
    <section class="features" id="sport">
      <div class="container">
        <div class="section-heading text-center">
          <h2>SPORT NEWS</h2>
          <p class="text-muted">Different sport events giving the best from our source!</p>
          <hr>
        </div>
        <div class="row">
          <div class="col-lg-4 my-auto">
            <div class="device-container">
              <div class="ads">
                <div class="device">
                  <div class="ads-background" style="background-size: 30px; border:solid; height: 400px; padding: 50px;">
                    <h3>Xclaimlabs</h3>
                    <p>Be secured with your site.</p>
                    <a href="http://www.xclaimlabs.com" style="text-decoration: none;">Click to connect</a>
                  </div>
                  <div class="button">
                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-money"></i>
                    <h3>Join and Bet</h3>
                    <p class="text-muted">Ready to start earning!</p>
                    <a href="http://www.bet.co.za" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <h3>Sale Offers</h3>
                    <p class="text-muted">Register and purchase online</p>
                    <a href="http://www.cashcrate.com/7517294" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-credit-card"></i>
                    <h3>MasterCard Security</h3>
                    <p class="text-muted">Secure your account!</p>
                    <a href="https://voguepay.com/
                    1766-0054927
                    " style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-film"></i>
                    <h3>Moviewatcher</h3>
                    <p class="text-muted">Watch any movie by downloading or streaming!</p>
                    <a href="http://www.bmovies.is" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Contact profile -->
    <section class="contact bg-primary" id="contact">
      <div class="container">
        <h2>I
          <i class="fa fa-heart"></i>
          new friends!</h2>
        <ul class="list-inline list-social">
          <li class="list-inline-item social-twitter">
            <a href="#">
              <i class="fa fa-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item social-facebook">
            <a href="#">
              <i class="fa fa-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item social-instagram">
            <a href="#">
              <i class="fa fa-instagram"></i>
            </a>
          </li>
        </ul>
      </div>
    </section>
    
    <!-- widget section -->
    <footer>
      <div class="container">
        <p>&copy; 2018 JoshyStandard.blog. All Rights Reserved.</p>
        <ul class="list-inline">
         <!--  <li class="list-inline-item">
            <a href="#">Privacy</a>
          </li>
          <li class="list-inline-item">
            <a href="terms.html">Terms</a>
          </li> -->
         <!--  <li class="list-inline-item">
            <a href="#">FAQ</a>
          </li> -->
        </ul>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/new-age.min.js"></script>


  </body>

</html>
