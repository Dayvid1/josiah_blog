<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>JoshyStandard.blog</title>

    
    <script src="js/instantSearch.js">
    /^#(?!\!)/.test(location.hash)&&(window.location.toString().replace(/#(?!\!)/,'#'));
    </script>
    
    <script>
      function encodeQ(param){
        return encodeURIComponent(param).replace(/'/g,"%27").replace(/%20|%2F/g, "+");
      }  
      function search(q){
        q=q.replace(/^\s+/,'').replace(/\s+$/,s '');
        $.mobile.changePage("/#!" + encodeQ(q));
        return false;
      }
    </script>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->
    <link href="css/new-age.min.css" rel="stylesheet">

    <script>
      if ($.support.fixedPosition) {
        $(document).on('pagechange',function(){
          $("#toTop").scrollToTop();
        });
      }
    </script>

    <!-- Analytics -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].1=1*new Date();a=s.createElement(0),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      $(document).on("pageshow", "[data-role=page]",function (event,ui){
        try{
          ga('create','UA-38415784-2','auto');
          hash=location.hash;
          if (hash) {
            ga('send', 'pageview', location.pathname+location.search+location.hash);
          }else{
            ga('send','pageview');
          }
        }catch(err){}
      });
    </script>

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Welcome To My Music Blog</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#download">DOWNLOAD</a>
            </li>
               <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="news.php">NEWS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#advertisement">ADVERTISEMENT</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">ABOUT</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">CONTACT</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead">
      <div class="container h-100">
        <div class="row h-100">
          <div class="col-lg-7 my-auto">
            <div class="header-content mx-auto">
            <style type="text/css" ></style>
              <h1 class="mb-5">Songs of old and current available, Make the choice to Download here!</h1>
              <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger">START SEARCH HERE!</a>
            </div>
          </div>
          <div class="col-lg-5 my-auto">
            <div class="device-container">
              <!-- <div class="device-mockup iphone6_plus portrait white"> -->
                <!-- <div class="device"> -->
                  <div class="screen">
                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                    <img src="josmuzik.jpg" class="img-fluid" alt="">
                  </div>
                  <div class="button">
                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                  </div>
                <!-- </div> -->
              <!-- </div> -->
            </div>
          </div>
        </div>
      </div>
    </header>


    <!-- Download profile -->
    <section class="download bg-primary text-center" id="download">
      <div class="container">
        <div class="row">
          <div class="col-md-8 mx-auto">
            <h2 class="section-heading">Find out the Song you desire to hear!</h2>
            <div class="content-main-player">
              <form class="searchForm" action="" onsubmit="return search(this.q.value)" method="get" data-ajax="false">
              <input type="search" class="searchQuery" name="q" id="searchField-player" value="" placeholder="Search Song.." autocomplete="off" data-prevent-focus-zoom="false" data-clear-btn-text=""/>
              <button type="submit">
                <i class="fa fa-search"></i>
              </button>
              <ul class="ac" id="suggestions" data-role="listview" data-inset="true"></ul>
              </form>
              <div id="search-ad728x90" style="width:320px;"></div>
              <div id="search-ad468x60" style="width:268px;"></div>
              <div id="search-ad320x50" style="width:150px;"></div>

              <main>
            
                <div id="searchResults">
                    
                </div>  
              </main>
            
            </div>
          </div>
        </div>
      </div>

      
    </section>

    <!-- Features profile -->
    <section class="features" id="advertisement">
      <div class="container">
        <div class="section-heading text-center">
          <h2>Unlimited Fun, Lovely Ads</h2>
          <p class="text-muted">Check out this Ads, its never too late to click a link!</p>
          <hr>
        </div>
        <div class="row">
          <div class="col-lg-4 my-auto">
            <div class="device-container">
              <div class="ads">
                <div class="device">
                  <div class="ads-background" style="background-size: 30px; border:solid; height: 400px; padding: 50px;">
                    <h3>Xclaimlabs</h3>
                    <p>Be secured with your site.</p>
                    <a href="http://www.xclaimlabs.com" style="text-decoration: none;">Click to connect</a>
                  </div>
                  <div class="button">
                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8 my-auto">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-money"></i>
                    <h3>Join and Bet</h3>
                    <p class="text-muted">Ready to start earning!</p>
                    <a href="http://www.bet.co.za" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <h3>Sale Offers</h3>
                    <p class="text-muted">Register and purchase online</p>
                    <a href="http://www.cashcrate.com/7517294" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-credit-card"></i>
                    <h3>MasterCard Security</h3>
                    <p class="text-muted">Secure your account!</p>
                    <a href="https://voguepay.com/
                    1766-0054927
                    " style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="feature-item">
                    <i class="fa fa-film"></i>
                    <h3>Moviewatcher</h3>
                    <p class="text-muted">Watch any movie by downloading or streaming!</p>
                    <a href="http://www.bmovies.is" style="text-decoration: none;">Right Here.</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="cta">
      <div class="cta-content">
        <div class="container">
          <h2>Be Free.<br>Be Bold.<br>Be Focused</h2>
         <!--  <a href="#contact" class="btn btn-outline btn-xl js-scroll-trigger">Let's move and !</a> -->
        </div>
      </div>
      <div class="overlay"></div>
    </section>

     <!-- About profile -->
    <section class="about" id="about">
      <div class="container">
        <div class="section-heading" style="text-align: center;">
          <h2>About us</h2>
          <i class="fa fa-3x fa-angle-down"></i>
        </div>
     </div>
      <div class="row"> 
        <div class="about-column col-lg-12">
          
          <div class="pull-left">
            <div class="col-lg-6">
            <div class="avatar">
            
              <img src="img/josiah.PNG" alt="avatar" class="img-circle" style="border-radius:25% width:150px; height: 200px;">
            </div>
            Blog Lead  
            </div>  
          </div>
             
          <div class="pull-right">
          <div class="col-lg-6">
            <div class="avatar">
            
              <img src="img/upcom.jpg" alt="avatar" class="img-circle" style="border-radius: 35% width:300px; height: 200px;">
            </div>
            Music Artist
          </div>  
      </div>
                      
    
    </section>

    <!-- Contact profile -->
    <section class="contact bg-primary" id="contact">
      <div class="container">
        <h2>I
          <i class="fa fa-heart"></i>
          new friends!</h2>
        <ul class="list-inline list-social">
          <li class="list-inline-item social-twitter">
            <a href="#">
              <i class="fa fa-twitter"></i>
            </a>
          </li>
          <li class="list-inline-item social-facebook">
            <a href="#">
              <i class="fa fa-facebook"></i>
            </a>
          </li>
          <li class="list-inline-item social-instagram">
            <a href="#">
              <i class="fa fa-instagram"></i>
            </a>
          </li>
        </ul>
      </div>
    </section>
    
    <!-- widget section -->
    <footer>
      <div class="container">
        <p>&copy; 2018 JoshyStandard.blog. All Rights Reserved.</p>
        <ul class="list-inline">
         <!--  <li class="list-inline-item">
            <a href="#">Privacy</a>
          </li>
          <li class="list-inline-item">
            <a href="terms.html">Terms</a>
          </li> -->
         <!--  <li class="list-inline-item">
            <a href="#">FAQ</a>
          </li> -->
        </ul>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/new-age.min.js"></script>


  </body>

</html>
